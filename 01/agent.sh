#!/bin/bash

yum -y install http://repo.zabbix.com/zabbix/3.2/rhel/7/x86_64/zabbix-release-3.2-1.el7.noarch.rpm
yum -y install zabbix-agent net-tools


rm -f /etc/zabbix/zabbix_agentd.conf
cp /vagrant/zabbix_agentd.conf /etc/zabbix/

systemctl start zabbix-agent
