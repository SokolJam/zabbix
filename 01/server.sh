#!/bin/bash

yum -y install mariadb mariadb-server vim net-tools

/usr/bin/mysql_install_db --user=mysql
systemctl start mariadb

mysql -uroot -e "create database zabbix character set utf8 collate utf8_bin; grant all privileges on zabbix.* to zabbix@localhost identified by 'Passw0rd';"

yum -y install http://repo.zabbix.com/zabbix/3.2/rhel/7/x86_64/zabbix-release-3.2-1.el7.noarch.rpm
yum -y install zabbix-server-mysql zabbix-web-mysql

zcat /usr/share/doc/zabbix-server-mysql-*/create.sql.gz | mysql -uzabbix -pPassw0rd zabbix

rm -f /etc/zabbix/zabbix_server.conf
cp /vagrant/zabbix_server.conf /etc/zabbix/

yum -y install zabbix-agent

systemctl start zabbix-server
systemctl start zabbix-agent 

sed -i 's/.*date\.timezone Europe.*/php_value date\.timezone Europe\/Minsk/' /etc/httpd/conf.d/zabbix.conf

echo -e '<VirtualHost *:80> \n DocumentRoot "/usr/share/zabbix" \n</VirtualHost>' > /etc/httpd/conf.d/vhost.conf

systemctl start httpd



